import React from 'react';
import Example from '../../assets/components/menu'

const Layout = (props) => {
    return(
        <React.Fragment>
            
            {props.children}
            <footer>
                <p>feito com amor</p>
            </footer>
        </React.Fragment>
    )
}

export default Layout;
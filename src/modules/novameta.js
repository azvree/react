import React from 'react';
import { Formik } from 'formik';
import '../assets/css/novameta.css';
import ButtonPrimary from '../assets/components/buttonprimary'
import API from '../assets/components/api/api'
import Example from '../assets/components/menu';
import MyApp from '../assets/components/calendar'

// const Basic = () => (
  const Basic = () => {
    
      API.post('').then(console.log);
    

  return (

    <React.Fragment>
      <Example/>
 <div className="containerPrincipal">   
    <div>
    <h1 className="h1" >Nova Meta</h1>
  </div>

    <div className="display spaceAround">
    <Formik
      initialValues={{ title: '', amount: '', category_id:'', color:'', description:'' }}
      onSubmit={(values,{setSubmitting}) => {
          API.post('/goals',values)
          console.log(values);
          setSubmitting(false);
       }}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
     
      }) => (
        <div className="form">
          <form className="column" onSubmit={handleSubmit}>

            <div className="container">
              <label>Titulo</label>
              <input className="input"
                  type="text"
                  name="title"
                  placeholder="Nome da meta"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.title}
                />
            </div>

            <div className="container">
              <label>Quantia:</label>
              <input className="input"
                type="text"
                name="amount"
                placeholder="Quantia em $ a economizar "
                onChange={handleChange}
                value={values.quantia}
              />
            </div>

            
            <div className="container">
              <label>Categoria:</label>
              <div className="display spaceBetween">
                  <select className="input"
                  type="text"
                  name="category_id"
                  placeholder="Nome da categoria"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.category}
                  >
                    <option value="13">viagem</option>
                    <option value="14">carro</option>
                    <option value="15">aviao</option>
                  </select>
                  <input className="inputMenor"
                  type="text"
                  name="color"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.color}
                  />
              </div>
            </div>

            <div className="container">
              <label>Descrição:</label>
              <textarea className="inputArea"
                type="textarea"
                name="description"
                onChange={handleChange}
                value={values.descricao}
              />
            </div>

            <button type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </form>
        </div> 
        
      )}
    </Formik>
    
    <div>
        <MyApp />
    </div>

  </div>


</div>  

  </React.Fragment>
  )};
      
export default Basic;

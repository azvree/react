import React from 'react';
import '../assets/css/pingbank.css';
import ButtonPrimary from '../assets/components/buttonprimary'
import {ReactComponent as Pig} from '../assets/img/OBJECT.svg'
import {ReactComponent as Idosa} from '../assets/img/idosa.svg'
import {ReactComponent as Mocaeidosa} from '../assets/img/mocaeidosa.svg'
import {ReactComponent as Stonks} from '../assets/img/stonks.svg'
import {ReactComponent as Pingde4} from '../assets/img/Pingde4.svg'
import Example from '../assets/components/menu'


const Home = () => {
    return(
    <React.Fragment>
        <Example />
    <section className="sectionPrincipal display column">

        <section className="display spaceAround">

            <div className ="display column spaceAround">
                <h1 className="h1">Ping Bank</h1>
                <p className="container p wrap">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus.</p>
                <ButtonPrimary path="/novameta" onClick={() => { }}>NOVA META </ButtonPrimary>
            </div>

            <Pig className="imagemPorco"/>
        </section>

        <section className="display spaceAround">

            <div className="display column">

                    <h2 className="h2">Como usar</h2>
                    <div className="display spaceBetween">

                        <div className="card display column center" >
                            <Idosa className="imagemComoUsar"/>
                            <h3 className="h3">Defina uma meta</h3>
                            <p className="container p wrap">Pellentesque faucibus leo sed arcu vulputate rhoncus.
                            Integer sit amet blandit felis.</p>
                        </div>

                        <div className="card display column center" >
                            <Stonks className="imagemComoUsar"/>
                            <h3 className="h3">Acompanhe o progresso</h3>
                            <p className="container p wrap center">Pellentesque faucibus leo sed arcu vulputate rhoncus.
                            Integer sit amet blandit felis.</p>
                        </div>

                        <div className="card display column  center" >
                            <Mocaeidosa className="imagemComoUsar"/>
                            <h3 className="h3">Alcance seu objetivo</h3>
                            <p className="container p wrap">Pellentesque faucibus leo sed arcu vulputate rhoncus.
                            Integer sit amet blandit felis.</p>
                        </div>
                    </div>  
                </div>     
        </section>

        <section>

            <div className="display spaceAround">

                <div className="column">
                    <h2 className="h2Metas">Já tem uma meta?</h2>
                    <p className="p container wrap">Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus,
                        commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</p>
                    <ButtonPrimary className="buton">Ver Metas</ButtonPrimary>
                    <h2 className="h2Metas">Não tem? Comece agora mesmo</h2>
                </div>

                <Pingde4 className="imagemPigde4"/>
            </div>

        </section>

    </section>
    </React.Fragment>
    );



};
export default Home; 

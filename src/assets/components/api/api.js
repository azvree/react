import React from 'react';
import axios from "axios";

const API = axios.create({
    baseURL: "https://in-pig-bank.herokuapp.com",
    responseType:"json"
});

export default API;
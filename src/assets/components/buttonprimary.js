import React from 'react';
import './buttonprimary.css'
import {BrowserRouter as Router,Link} from 'react-router-dom'

const  ButtonPrimary = (props) => {
    return(
           
        <Link to={props.path}><button className="button" onClick={props.buttonFunction}>{props.children}</button></Link>
    
    );
    
};

export default ButtonPrimary;
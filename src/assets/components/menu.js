import React from 'react';
import {BrowserRouter as Link, Switch, Route} from 'react-router-dom'
import { slide as Menu } from 'react-burger-menu';
import './menu.css'


const Example = () =>  {
  const showSettings = (event) => {
    event.preventDefault();
  }
    return (
      <React.Fragment>
      <Menu className="menu">
            <Link exact path to="/" id="home" className="menu-item">Home</Link>
            <Link exact path to='/novameta' className="menu-item">Nova meta</Link>
      </Menu>
      </React.Fragment>
    );
  
}

export default Example;
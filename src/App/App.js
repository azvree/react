
import React from 'react';
import './App.css';
import Home from '../modules/home'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Layout from '../hoc/layout/Layout'
import Basic from '../modules/novameta'

function App() {
  return (
    <Layout>
      <Router>
        <Switch>
          <Route exact path="/" ><Home/></Route>
          <Route exact path="/novameta" ><Basic/></Route>
          <Route exact path="/meta" ><p>Embreve</p></Route> 
          <Route exact path="*" ><p>quatrocentoequadro</p></Route> 
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;
